package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

const usage = `NAME:
  missing_uuids - Scan a gemnasium-db repo, list advisories that have no UUID

USAGE:
  missing_uuids <gemnasium-db-dir>`

func main() {

	// parse arguments
	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Println(usage)
		os.Exit(1)
	}

	// collect paths of all advisories
	root := flag.Arg(0)
	advisories := []string{}
	for _, pkgType := range packageTypes() {
		pkgDir := filepath.Join(root, pkgType)
		paths, err := findAdvisories(pkgDir)
		if err != nil {
			panic(err)
		}
		advisories = append(advisories, paths...)
	}

	// look for UUIDs, build a list of faulty advisories
	faulty := []string{}
	for _, a := range advisories {
		ok, err := advisoryHasUUID(a)
		if err != nil {
			panic(err)
		}
		if !ok {
			faulty = append(faulty, a)
		}
	}

	// print report
	if len(faulty) > 0 {
		fmt.Println("some advisories have not been published:")
		for _, a := range faulty {
			fmt.Println(a)
		}
		os.Exit(1)
	} else {
		fmt.Println("all advisories have not been published")
	}
	os.Exit(0)
}

func packageTypes() []string {
	return []string{
		"gem",
		"maven",
		"npm",
		"packagist",
		"pypi",
	}
}

// findAdvisories returns the paths of all the advisories
// for in a directory that corresponds to a package type.
func findAdvisories(path string) ([]string, error) {
	paths := []string{}

	// in sub-directories
	depth1, err := filepath.Glob(path + "/*/*.yml")
	if err != nil {
		return nil, err
	}
	paths = append(paths, depth1...)

	// in sub-sub-directories
	depth2, err := filepath.Glob(path + "/*/*/*.yml")
	if err != nil {
		return nil, err
	}
	paths = append(paths, depth2...)

	return paths, err
}

// advisoryHasUUID tells if the given advisory has a UUID.
func advisoryHasUUID(path string) (bool, error) {
	f, err := os.Open(path)
	if err != nil {
		return false, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		var fields = strings.Fields(scanner.Text())
		if len(fields) > 1 && fields[0] == "uuid:" {
			return true, nil
		}
	}
	if err := scanner.Err(); err != nil {
		return false, err
	}
	return false, nil
}
